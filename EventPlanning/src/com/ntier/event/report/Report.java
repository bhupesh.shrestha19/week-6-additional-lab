package com.ntier.event.report;
import com.ntier.event.test.RentalManager;

/**
 * 
 * @author Bhupesh shrestha
 * Date: 09/17/2020
 * Lab 8.5 Displaying Data
 * 
 */

public class Report {
	
	//for Lab 10.6
	public static final String footer = "Copyright Event Management Gurus 2013";

	public static void main(String[] args) {
		RentalManager report = new RentalManager();
		
		//calling method from RentalManager class
	//	report.displayCars();
	//	report.displayRooms();
		
		
		//calling static method from RentalManager class
		//for lab 10.5
		RentalManager.displayCars();
		RentalManager.displayRooms();
	
	}

}

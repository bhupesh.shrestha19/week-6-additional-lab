package com.ntier.event.util;

import com.ntier.event.rental.RentalCar;
import com.ntier.event.reservation.CarReservation;

public class EmailSender extends CarReservation {
	
	public EmailSender(String licenseNumber) {
		super(licenseNumber);
	}
	
	
	public String emailSender() {
		if(this.getLicenseNumber().length() >= 11) {
			return "true";
		}
		else {
			return "false";
		}
		
	}
	
}

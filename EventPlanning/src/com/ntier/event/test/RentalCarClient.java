package com.ntier.event.test;
import com.ntier.event.rental.RentalCar;
import com.ntier.event.reservation.CarReservation;
import com.ntier.event.util.EmailSender;

/**
 * 
 * @author Bhupesh shrestha
 * Date: 09/17/2020
 * Lab 8.1 Data Members in Classes
 * Lab 8.2 Accessor Methods 
 * Lab 8.3 Representing an Object's Data
 * Lab 10.2 Constructor
 * 
 */

public class RentalCarClient {

	public static void main(String[] args) {
		// for lab 8.1
		
 //       RentalCar car1 = new RentalCar(); 
 //       car1.id = 123l;
 //       car1.make = "Nissan";
 //       car1.model = "Maxima";
        
 //		System.out.println("RentalCar: id=" + car1.id + " make=" + car1.make 
 // 				+ " model=" + car1.model);
 //	}
		
		//for lab 8.2
		//using getters and setters
//		RentalCar car2= new RentalCar();
//		car2.setId(123l);
//		car2.setMake("Nissan");
//		car2.setModel("Maxima");
		
		//for lab 10.2
	//	RentalCar car2= new RentalCar(123l);
		
		// for lab 8.3 using getMethod()
//		System.out.println("RentalCar: id=" + car2.getId() + " make=" + car2.getMake() 
//					+ " model=" + car2.getModel());
		
		
		//passing license number which is not 12 character
		CarReservation cr = new CarReservation("Mlv234");
        
		
		CarReservation cr1 = new CarReservation("Mlv123456789");
		
		
		EmailSender es1 = new EmailSender("Mlv234");
		String emailSender1 = es1.emailSender();
		
		EmailSender es2 = new EmailSender("Mlv123456789");
		String emailSender2 = es2.emailSender();
		
		System.out.println(emailSender1);
		System.out.println(emailSender2);

	}

}
		



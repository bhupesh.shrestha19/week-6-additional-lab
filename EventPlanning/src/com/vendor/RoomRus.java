package com.vendor;

import com.ntier.event.reservation.HotelReservation;
/**
 * 
 * @author Bhupesh Shrestha
 * Date: 9/27/2020
 * Lab 11.3 Making Reservations
 *
 */

public class RoomRus extends HotelReservation {

	public RoomRus(String hotelClubNumber) {
		super(hotelClubNumber);
	
	}
	

   public char[] roomRu() {
		if(this.getHotelClubNumber().charAt(0) == 'N' && this.getHotelClubNumber().length() >= 7) {
			return "%^&valid".toCharArray();
          }
		else {
		    return "%($invalid".toCharArray();
		  }
   }

}
